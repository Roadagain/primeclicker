function show(id){
  'use strict';

  document.getElementById(id).style.display = 'block';
}

function hide(id){
  'use strict';

  document.getElementById(id).style.display = 'none';
}

function clear(id){
  'use strict';

  document.getElementById(id).innerHTML = '';
}

function generatePrime(max){
  'use strict';

  var prime = [];
  var notPrime = [1];

  // make prime and not-prime arrays
  for (var i = 2; i <= max; i++){
    var isPrime = true;
    for (var j = 0; j < prime.length; j++){
      if (i % prime[j] === 0){
        notPrime.push(i);
        isPrime = false;
        break;
      }
    }
    if (isPrime){
      prime.push(i);
    }
  }

  return function(isPrime){ // closure
    if (isPrime){
      // ONLY ONE PRIME
      return prime[Math.random() * prime.length | 0];
    }
    else {
      var pos = Math.random() * notPrime.length | 0;
      var value = notPrime[pos];
      notPrime.splice(pos, 1); // remove choosed element
      return value;
    }
  };
}

function setResult(message){
  'use strict';

  document.getElementById('result').innerHTML = String(message);
}

function setNumbers(n, max, level, time){
  'use strict';

  //clear table and result
  clear('numbers');
  clear('result');

  var numbers = document.getElementById('numbers');

  var primePosition = Math.random() * n[level] * n[level] | 0;
  var getNumber = generatePrime(max[level]); // get a closure what returns a random choosed number
  var start = new Date();
  for (var i = 0; i < n[level]; i++){
    var tr = numbers.appendChild(document.createElement('tr'));
    for (var j = 0; j < n[level]; j++){
      var td = tr.appendChild(document.createElement('td'));
      td.innerHTML = String(getNumber(i * n[level] + j === primePosition));

      // add eventlistener
      if (i * n[level] + j === primePosition){
        // accepted
        td.id = 'correct';
        td.addEventListener('click', function(){
          var end = new Date();
          if (level >= 2){
            var record = (time + (end - start)) / 1000;
            if (level === 3){
                // double record if it is made-in-heaven mode
                record *= 2;
            }
            setResult('Conguratulations!!<br>Your record is ' + record + ' sec!!');
            window.setTimeout(function(){
              hide('start');
              show('madeInHeaven');
              show('descriptionMadeInHeaven');
            }, 1000);
          }
          else {
            setNumbers(n, max, level + 1, time + (end - start));
          }
        });
      }
      else {
        // wrong answer
        td.addEventListener('click', function(){
          document.getElementById('correct').style.color = 'red';
          setResult('Failed...');
        });
      }
    }
  }
}

window.addEventListener('load', function(){
  'use strict';

  var n = [2, 3, 4, 5];
  var max = [9, 49, 99, 256];
  document.getElementById('start').addEventListener('click', function(){
    hide('description');
    setNumbers(n, max, 0, 0);
  });
  document.getElementById('madeInHeaven').addEventListener('click', function(){
    // hell
    hide('descriptionMadeInHeaven');
    setNumbers(n, max, 3, 0);
  });
});
